/*------------------------------------TAAK2------------------------------------------------*/
//Esat Ergunes 
//esat.ergunes@student.ehb.be
//2018-2019
//MULTEC

////////////////////////////////Bronnen//////////////////////////////////////////////////

/*
-Cursus
-Oefeningen
-Internet(Youtube,Blogs)
 */

/*---------------------------------------------------------------------------------------*/
PFont font1;

//Coördinates of cube
int middleX, middleY, top, bottom, left, right, topCorner, bottomCorner;

//Variables for myAnimation HEY
int size = 20;

boolean playMyAnimation = false;
boolean playMyAnimation2 = false;
boolean playMyAnimation3 = false;
boolean playMyAnimation4 = true;
boolean playMyAnimation5 = false;



void setup() {

  size(1280, 720); 
  colorMode(HSB, 360, 100, 100, 100);
  setGlobalVariables();
  initializeMinim();
  font1=loadFont("HelveticaNeue-Light-48.vlw");
}

void draw() {

  //White Cube planes
  background(0, 0, 100);

  //checks each frame if there is a beat.
  beat.detect(player.mix);

  drawAnimations();
  drawCubeMask();
  startTextDisplay();
}

void keyReleased() {
  //toggle Animations ON or OFF
  if (key == 'u') {

    playMyAnimation = !playMyAnimation;
    playMyAnimation2 =false;
    playMyAnimation3 =false;
    playMyAnimation4=!playMyAnimation;
    playMyAnimation5=!playMyAnimation5;
  }
  if  (key=='i') {
    playMyAnimation5=false;
    playMyAnimation4=false;
    playMyAnimation =false;
    playMyAnimation3 =false;
    playMyAnimation2 =true;
  }
  if (key=='o') {
    playMyAnimation5=false;
    playMyAnimation4=false;
    playMyAnimation3 = true;
    playMyAnimation2 =false;
    playMyAnimation =false;
  }
}

void drawAnimations() {  
  //play animation if toggled ON
  if (playMyAnimation == true) {
    myAnimation();
  } else if (playMyAnimation2==true) {
    SecondAnimation();
  } else if (playMyAnimation3==true) {
    thirdanimation();
  }
}

void startTextDisplay() {
  if (playMyAnimation4==true) {
    startmessage();
  }
  if (playMyAnimation5==true ) {

    moveMouse();
  }
}
/*------------------------------------------------------CODE-1ste-ANIMATIE----------------------------------------------------*/
void myAnimation() {
  if (beat.isOnset()) {
    size = 20;
  }

  for (int i = 0; i < 360; i+=100) {
    float Hoek = sin(radians(i*50-frameCount*50))*1;
    if (Hoek >1) {
      stroke(0);
    } else {
      stroke(25, 100, 100);
      strokeWeight(random(i%frameCount));
    }

    fill(mouseX%255, mouseY%255, 255);
    ellipse(middleX, middleY, size-2, size-2);
    size+= 20;
  }
}


/*------------------------------------------------------CODE-2de-ANIMATIE----------------------------------------------------*/
int n=0;
int c=2;

void SecondAnimation() {
  if (beat.isOnset()) {
    size = 20;
  }


  for (int i = 0; i < 360; i+=100) {
    float Hoek = sin(radians(i/50-frameCount/50))*1;
    if (Hoek > 300) {
      strokeWeight(Hoek);
    } else {
      stroke(255);
      strokeWeight(random(100));
    }
    fill(0);
    ellipse(middleX, middleY, size/3, size/3);
    size+= i;

    rectMode(CENTER);
    rect(middleX, middleY, size/6, size/6);
    size+= i;
  }
}

/*------------------------------------------------------CODE-3de-ANIMATIE----------------------------------------------------*/
void thirdanimation() {
  if (beat.isOnset()) {
    size = 20;
  }
  noFill();

  for (int d = 360; d > 0; d -= 100) {
    background(0);
    stroke(255);
    strokeWeight(random(5));
    ellipse(middleX, middleY, d+size*6%frameCount, d+size%frameCount);
    strokeWeight(random(3));
    stroke(frameCount % 300, 100, 100);
   

    ellipse(middleX, middleY, d-size*6%frameCount, d-size%frameCount);
 
}
 
   size=size+10;
  
  
  
 
}



/*------------------------------------------------------CODE-animatie-Text---------------------------------------------------*/
void startmessage() {


  rectMode(CENTER);
  stroke(frameCount % 300, 100, 100);


  strokeWeight(2);
  rect(middleX, middleY, 940, 70);
  textFont(font1, 50);
  fill(frameCount % 300, 100, 100);

  rectMode(CENTER);
  text("Press the  u, i  or  o  keys to start animations", middleX-448, middleY+15);
}

void moveMouse() {
  textFont(font1, 25);
  fill(255);
  text("Move the mouse to change color", middleX-610, middleY+330);

  strokeWeight(3);
  stroke(frameCount%255*1);
  noFill();
  rectMode(CENTER);
  rect(202, 680, 380, 50);
}